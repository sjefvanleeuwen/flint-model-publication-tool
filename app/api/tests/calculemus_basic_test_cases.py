# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
Abstract classes for testing posting, retrieval, and list retrieval.

Author: Jeroen Breteler
Author affiliation: TNO
Date: 2020-03-03
"""

from django.urls import reverse
from rest_framework import status as http_status

from .CalculemusTestCase import CalculemusTestCase
from .testdata import testdata

class CalculemusBasicTestCase(CalculemusTestCase):
    """
    Test posting and single/list retrieval of object.

    This inherits from CalculemusTestCase to manage user authorization.
    
    To use this class for a particular asset type, subclass it and fill in
    the appropriate label.
    """

    # Values that need to be instantiated by subclasses
    label = '' # E.g. "acts", "facts", "duties"

    def _test(self):
        # Get the testdata
        test_assets = testdata.data[self.label]['valid']
        # Post each of the data instances
        asset_ids = []
        for title, data in test_assets.items():
            print("Testing post of case {} with data:\n{}\n".format(title,data))
            url=reverse('{}_list'.format(self.label))
            asset_id = self.post_object(url, data)
            print("Posted case \"{}\" with ID {}".format(title, asset_id))
            asset_ids.append(asset_id)
            # Retrieve a single asset
            get_url = reverse('{}_detail'.format(self.label),
                kwargs={'asset_id':asset_ids[0]})
            asset = self.get_object(get_url)
            print("Get URL is {}".format(get_url))
            print("Retrieved asset with ID {}. Contents: {}".format(asset_ids[0], asset))
            # Try retrieving all assets,
            # Authorize as user that posted the assets
            self.authorize('Robert')
            # assert length equals length of assets
            print("Retrieving all assets")
            list_url = reverse('{}_list'.format(self.label))
            response = self.client.get(list_url)
            if response.status_code != http_status.HTTP_200_OK:
                print("Could not retrieve list: {}".format(response.data))
            self.assertEqual(response.status_code, http_status.HTTP_200_OK)
            # Check that all assets were found,
            # by comparing sizes of the collections
            retrieved_assets = response.data
            self.assertEqual(len(asset_ids), len(retrieved_assets))
