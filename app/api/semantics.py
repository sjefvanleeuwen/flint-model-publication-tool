# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
Methods to query and process RDF objects.
"""
import rdflib
import os
import json
from rdflib.namespace import RDF, XSD
import re

BDB_NS = "bdb://"


def has_type(type, asset):
    """
    This function takes a semantic type @type and an asset @asset
    and answers whether the rdf of @asset contains any subject
    carrying @type.
    """

    # Check that the asset is stripped down to its data
    if 'data' in asset:
        asset = asset['data']
    return Semantics().triple_exists(asset['rdf'], (None, Semantics.RDF.type, type))

class Semantics:
    NODE_ID = "{}[id]/".format(BDB_NS)
    placeholder_pattern = r"{}\[id\]".format(BDB_NS)
    project_namespace = os.getenv('PROJECT_NAMESPACE', 'http://ontology.tno.nl/example#')
    NS = rdflib.Namespace(project_namespace)
    # Make RDF namespace part of Semantics class
    RDF = RDF

    context = {
        "flint": project_namespace,
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
    }
    # Construct NamespaceManager
    nsMgr = rdflib.namespace.NamespaceManager(rdflib.Graph())
    for prefix, ns in context.items():
        nsMgr.bind(prefix, rdflib.URIRef(ns))

    '''
    Utility functions
    '''
    @classmethod
    def load_rdf(cls, serialized_data, data_format='json-ld'):
        g = rdflib.ConjunctiveGraph()

        try:
            g.parse(data=serialized_data, format=data_format)
        except:
            return False

        return g

    @classmethod
    def triple_exists(cls, data, triple=(None, None, None)):
        g = cls.load_rdf(json.dumps(data))

        def castValue(val):
            """
            Cast the values of the triple in terms
            that rdflib understands, using `from_n3`.
            Crucially, from_n3 only builds literals
            if the values are in "" quotes, so arguments
            should be formatted before calling `triple_exists`.

            From_n3 also doesn't handle terms that are already nodes,
            so those must not be passed through from_n3 again.
            """
            if isinstance(val, rdflib.term.Node):
                return val
            else:
                return rdflib.util.from_n3(val, nsm=cls.nsMgr)

        triple = tuple(map(castValue, triple))
        # print("Searching for triple: {}".format(triple))

        for s, p, o in g.triples(triple):
            return True
        return False

    @classmethod
    def get_nodes(cls, data):
        g = cls.load_rdf(json.dumps(data))

        nodes = []
        for n in g.all_nodes():
            if isinstance(n, rdflib.term.URIRef):
                if re.match(cls.placeholder_pattern, n):
                    for s, p, o in g.triples((n, RDF.type, None)):
                        nodes.append((s, o))
        return nodes

    ###################
    # Project-specific
    ###################
    @classmethod
    def serialize_model(cls, asset_id_dict):
        """
        Given a list of asset IDs of various types, output a FLINT model as RDF
        
        The asset_id_dict contains keys indicating the various asset types.
        """
        # Create result object
        graph = rdflib.Graph()

        # Create root node representing model
        model = rdflib.URIRef(cls.NODE_ID)
        # Assign type
        graph.add((model, cls.RDF.type, cls.NS.Model))
        # Include all assets
        for asset_type, asset_ids in asset_id_dict.items():
            for asset_id in asset_ids:
                asset_node = rdflib.URIRef(BDB_NS + asset_id)
                graph.add((model, cls.NS.contains, asset_node))
        # Return serialized version
        return graph.serialize(format="json-ld", context=cls.context)