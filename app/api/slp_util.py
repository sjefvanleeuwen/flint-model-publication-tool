# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
A module containing utility functions for interaction with an SLP instance.
Actual communication is performed in slp_interface.py.
The present module captures common or intuitive usage patterns of the SLP interface.
"""
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework import status as http_status

from api.slp_interface import SlpInterface

from api.models import SlpId
import api.semantics as semantics

import os

# Simple wrapper functions
def get_asset(*args, **kwargs):
    return SlpInterface().get_asset(*args, **kwargs)

def transfer(*args, **kwargs):
    return SlpInterface().transfer(*args, **kwargs)

def validate(*args, **kwargs):
    return SlpInterface().validate_publication(*args, **kwargs)

def publish_as_user(user, payload, shape_id, **kwargs):
    """Get SLP info from user and forward to publish()."""
    slp_id_object = SlpId.objects.filter(user=user, active=True).order_by('-timestamp')[0]
    slp_id = slp_id_object.slp_id
    private_key = slp_id_object.private_key

    return SlpInterface().publish(
        slp_id=slp_id,
        private_key=private_key,
        payload=payload,
        shape=shape_id,
        **kwargs
    )
    
def all_assets(user, type=None, history=False):
    """
    Return all assets owned by a user.

    @type: an optional way of filtering for assets of a particular type.
    @history: if True, returns all assets the user had some involvement in
    (i.e. was at some point owner of).
    """
    slp_interface = SlpInterface()

    # Retrieve user's SLP IDs
    try:
        slp_ids = SlpId.objects.filter(user=user, active=True)
        # Extract the actual SLP-ID string from the complex SLP-ID object
        slp_ids = [id.slp_id for id in slp_ids]
    except ObjectDoesNotExist:
        # TODO any special exception handling?
        raise ObjectDoesNotExist("Could not retrieve user's SLP IDs")

    # collect assets
    assets = []
    for slp_id in slp_ids:
        try:
            if history:
                # Get all transactions in a user's history, grouped by asset
                assetDicts = slp_interface.get_history_of_user(slp_id)
            else:
                # Send asData=True flag, else this only returns asset IDs!
                assetDicts = slp_interface.get_assets_of(slp_id, asData=True)
        except Exception as e:
            #TODO handle exception type?
            raise e
        
        # Extract only the assets
        for assetID, assetDict in assetDicts.items():
            asset = assetDict['asset']
            if 'id' not in asset:
                # CREATE transactions do not insert an 'id' field...
                asset['id'] = assetID
            assets.append(asset)

    if type:
        assets = [asset for asset in assets 
                    if semantics.has_type(type, asset)]
    
    return assets

def owns(user, asset_id):
    """
    Determine if an asset exists and
    whether it is owned by a given user.
    """

    # Create interface to SLP
    slp_interface = SlpInterface(
        os.getenv('SLP_URL'),
        os.getenv('SLP_TOKEN')
    )

    # Retrieve asset
    try:
        asset = slp_interface.get_asset(asset_id)
    except ValueError:
        # The asset does not exist
        return False
    
    # Retrieve user's SLP IDs
    try:
        slp_ids = SlpId.objects.filter(user=user, active=True)
        # Extract the actual SLP-ID string from the complex SLP-ID object
        slp_ids = [id.slp_id for id in slp_ids]
    except ObjectDoesNotExist:
        # TODO any special exception handling?
        return False
    
    # For each SLP ID, check if the asset is among the
    # assets owned by that ID
    for slp_id in slp_ids:
        try:
            user_assets = slp_interface.get_assets_of(slp_id)
        except Exception as e:
            # TODO handle some exceptions differently? E.g. bad request could be passed on.. 
            print('Cannot check asset ownership:', str(e))
            continue
        for user_asset in user_assets:
            if asset_id == user_asset['asset_id']:
                return True

    # If no to the above, the user does not own the asset
    return False


def previousOwner(asset_id):
    """
    For a given asset, retrieve its owner before it was
    last transferred.
    """

    # Create interface to SLP
    slp_interface = SlpInterface(
        os.getenv('SLP_URL'),
        os.getenv('SLP_TOKEN')
    )

    # TODO BUG if this is called with asset ID it can only check the owner making the original transaction;
    # subsequent transfers are not registered, as the tx_id is different from the asset_id.
    asset_inputs = slp_interface.get_inputs(asset_id)

    # Verify inputs
    if not len(asset_inputs) == 1:
        return Response('Asset has multiple inputs',
                        status=http_status.HTTP_400_BAD_REQUEST)

    if not len(asset_inputs[0]['owners_before']) == 1:
        return Response('Asset is multisig asset',
                        status=http_status.HTTP_400_BAD_REQUEST)

    prev_owner = asset_inputs[0]['owners_before'][0]
    return prev_owner

def validate_all(asset_ids):
    return [
        id for id in asset_ids if validate(id)==True
    ]
    
