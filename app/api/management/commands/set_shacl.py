# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
import os
import re
from django.db import DatabaseError
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from api.models import Setting, SlpId
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime # For time stamping

from rest_framework.test import APIClient
from django.urls import reverse
from rest_framework import status as http_status

from django.conf import settings    # To get BASE_DIR


def post_shape(shape_name, shape_filepath, client, admin_slp_id):
    """
    Read in and post a single SHACL file, returning asset ID
    """
    # read shape file
    with open(shape_filepath) as f:
        shape_content = f.read()

    # publish shape
    url = reverse('raw_publication')
    postdata = {
        "publication": shape_content,
        "format": "n3",
        "description": "{} SHACL shape @ {}".format(shape_name, str(datetime.now)),
        "slp_id": admin_slp_id
    }
    response = client.post(url, data=postdata)

    if response.status_code != http_status.HTTP_200_OK:
        raise CommandError("Failed to publish shape: " + response.data)
    
    shape_asset_id = response.data
    print(shape_name, 'shape asset ID:', shape_asset_id)

    return shape_asset_id


def save_shape_setting(shape_name, shape_asset_id, admin_username):
    """
    Save a shape's asset ID in the Setting data model
    """
    # Define the setting's label, based on shape name
    setting_label = shape_name + "_shape"
    # If the setting already exists, update it
    try:
        s = Setting.objects.get(setting=setting_label)
        print("{} setting exists, replacing asset ID {} by {}".format(
            setting_label, s.value, shape_asset_id
        ))
        s.value = shape_asset_id
        s.save()
    # If the setting does not exist, create it
    except ObjectDoesNotExist:
        s = Setting(user=User.objects.get(username=admin_username), setting=setting_label, value=shape_asset_id)
        try:
            s.save()
        except DatabaseError as e:
            raise CommandError("Unable to save setting: {}".format(e))

def find_shape_files(shape_dir=None):
    """
    Return a dict of all relevant shapes, by name and filepath

    shapes = {
        "<shape_name1>":"<shape1 filepath>",
        ...
    }
    """
    # Create collector for results
    shapes = {}
    # Set shape directory
    if not shape_dir:
        shape_dir = os.path.join(settings.BASE_DIR, 'api/semantics/')
    # Find SHACL files
    # Shacl file regex pattern
    pattern = "(?P<shape_name>.+)_shape\.ttl"
    for filepath in os.listdir(shape_dir):
        match = re.match(pattern, filepath)
        if match:
            shape_name = match.group("shape_name")
            shapes[shape_name]=os.path.join(shape_dir, filepath)
    return shapes



class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--admin-username',
                            help="The name of the account holding admin permissions")

    def handle(self, *args, **options):
        """
        This command posts all relevant SHACL shapes to the ledger
        and sets the Setting key-value pairs accordingly.

        The command requires the use of a User account with admin permissions.
        @admin_username: A username for a user with admin permissions.
        """        
        if 'admin_username' in options and options['admin_username']:
            admin_username = options['admin_username']
        else:
            admin_username = 'root'        
        print('Posting SHACL with username', admin_username)
        # Create client for POST calls
        client = APIClient()
        # Get admin info
        try:
            admin_token = Token.objects.get(user__username=admin_username)
            # get most recent, active, slp-id
            admin_slp_id = SlpId.objects.filter(user__username=admin_username, active=True).order_by('-timestamp')[0].slp_id
        except ObjectDoesNotExist as e:
            raise CommandError("Unable to retrieve admin user info: {}".format(e))
        # Authenticate as admin
        client.credentials(HTTP_AUTHORIZATION='Token ' + admin_token.key)

        # Get all shapes
        shapes = find_shape_files()
        # Loop over shapes, post each one
        for shape_name, shape_filepath in shapes.items():
            # Post shape
            shape_asset_id = post_shape(shape_name, shape_filepath, client, admin_slp_id)
            # Save shape asset ID
            save_shape_setting(shape_name, shape_asset_id, admin_username)
        print("All SHACL shapes set")
