# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
Module to handle calls to the RML mapper.

Classes: RMLInterface
"""
import requests
from rest_framework.status import HTTP_200_OK
import json
import os

class RMLInterface(object):
    """
    Defines methods for interacting with the RML mapper web service.
    """
    port = os.getenv("RML_PORT", "4000")
    hostname = os.getenv("RML_SERVICE", "http://rml")
    
    @classmethod
    def map_json(cls, data_json, rml, serialization="jsonld"):
        """
        Perform a call to map json data to RDF.
        """
        # Set up variables for the call
        url = "{hostname}:{port}/execute".format(hostname=cls.hostname, port=cls.port)
        headers = {"Content-Type":"application/json"}
        data = {
            "rml":rml,
            "sources": {
                "data.json":json.dumps(data_json)
            },
            "serialization":serialization
        }
        import sys
        sys.stdout.flush()
        # Send the POST call
        response = requests.post(url, data=json.dumps(data), headers=headers)
        if response.status_code != HTTP_200_OK:
            error_message = "Error during RML mapping (HTTP{}): {}"
            error_message = error_message.format(response.status_code, response.text)
            raise RuntimeError(error_message)
        # Status==200
        # Unpack two layers of string encoding of json
        # Get the response content..
        response_json = json.loads(response.content)
        # return response_json
        # Get the JSON that is behind the 'output' key in the content
        response_output = response_json['output']
        # Check that output is not empty
        if not response_output:
            # Output is an empty list
            print("Warning: RML mapping returned an empty string.")
            return json.loads("{}")
        else:
            return json.loads(response_output)