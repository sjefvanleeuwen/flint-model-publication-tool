# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from .SLPView import SLPView, SLPListView, SLPDetailView

class ActView(SLPView):
    def asset_name(self):
        return "Act"

class ActListView(ActView, SLPListView):
    pass

class ActDetailView(ActView, SLPDetailView):
    pass