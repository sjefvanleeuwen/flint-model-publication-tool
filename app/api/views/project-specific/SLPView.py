# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
Module defining the abstract SLPView class and its children SLPListView and SLPDetailView,
which may be used by generated views.

Classes: SLPListView, SLPDetailView

Author: Jeroen Breteler
Date created: 2020-01-20
"""
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status as http_status

import api.slp_util as slp_util
import api.semantics as semantics
from api.semantics import Semantics
import api.logic as logic

from api.models import Setting
from django.core.exceptions import ObjectDoesNotExist

from api.rml_interface import RMLInterface as RML
from api.slp_interface import SlpInterface as SLP
import api.slp_util as slp_util

import os
import json
from django.conf import settings

# TODO do we need authentication classes/permissions?

class SLPView(APIView):
    """
    Abstract class,
    to be inherited by
    SLPListView and SLPDetailView.
    """

    def asset_name(self):
        """Determine the name of the asset."""
        pass

    def get_shape_id(self):
        """Retrieve SHACL asset ID for this asset type."""
        try:
            shape_setting = Setting.objects.get(setting=self.asset_name().lower() + "_shape")
            shape_id = shape_setting.value
            return shape_id
        except ObjectDoesNotExist:
            raise ValueError("No SHACL set for {} object".format(self.asset_name()))

class SLPListView(SLPView):
    """
    An abstract class for views.
    GET: Return a list of a certain type of object.
    POST: Create a new object of a certain type.
    """

    def get(self, request):
        """Retrieve a list of the user's assets of the right type."""
        user = request.user
        asset_type = semantics.Semantics().NS[self.asset_name()]
        # Get all assets of the type
        all_assets = slp_util.all_assets(user, type=asset_type)
        # Determine valid asset IDs
        valid_asset_ids = slp_util.validate_all([asset['id'] for asset in all_assets])
        # Filter by valid IDs
        all_assets = [asset for asset in all_assets if asset['id'] in valid_asset_ids]
        return Response(all_assets, status=http_status.HTTP_200_OK)

    def post(self, request):
        """
        Publish an object on the ledger.

        1. Receive JSON
        2. Map JSON to RDF using RML
        3. Publish on the SLP to trigger SHACL
        """
        rml_rel_path = 'api/semantics/{name}_rml.ttl'
        rml_rel_path = rml_rel_path.format(name=self.asset_name().lower())
        rml_path = os.path.join(settings.BASE_DIR, rml_rel_path)
        rml = open(rml_path, 'r').read()
        # JSON is in the request
        data_json = request.data
        # Map JSON => RDF
        rdf = RML().map_json(data_json, rml)
        # Debug
        # print("Result from RDF mapping: \n{}\n".format(rdf))
        # return Response(rdf, status=http_status.HTTP_200_OK)
        # Publish to the SL platform
        # Publication will trigger SHACL validation
        try:
            asset_id = slp_util.publish_as_user(
                user=request.user,
                payload=json.dumps(rdf),
                shape_id=self.get_shape_id()
            )
        except RuntimeError as e:
            return Response("Could not publish: {}".format(e),
                status=http_status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(asset_id, status=http_status.HTTP_200_OK)

class SLPDetailView(SLPView):
    """
    An abstract class for views.

    GET: Return a single view.
    """
    def get(self, request, asset_id):
        """Return a single asset's contents"""
        # Check logic and return any response if the check fails
        logic_response = logic.checkLogic([
            (logic.asset_exists, [asset_id]),
            (logic.asset_has_type, [asset_id, Semantics().NS[self.asset_name()]]),
            (logic.asset_is_valid, [asset_id]),
        ])
        if logic_response: return logic_response
        # Retrieve asset
        asset = slp_util.get_asset(asset_id)
        # Pass to user.
        return Response(asset, status=http_status.HTTP_200_OK)