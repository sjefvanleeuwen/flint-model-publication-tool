# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from django.urls import path, re_path

from . import ActViews, DutyViews, FactViews, ModelViews
from api.standards.RegexPatterns import asset_id

urlpatterns=[
    # Acts
    re_path('^acts/?$', ActViews.ActListView.as_view(), name='acts_list'),
    re_path(
        '^acts/(?P<asset_id>{regex})/?$'.format(regex=asset_id),
        ActViews.ActDetailView.as_view(), name='acts_detail'),
    # Duties
    re_path('^duties/?$', DutyViews.DutyListView.as_view(), name='duties_list'),
    re_path(
        '^duties/(?P<asset_id>{regex})/?$'.format(regex=asset_id),
        DutyViews.DutyDetailView.as_view(), name='duties_detail'),
    # Facts
    re_path('^facts/?$', FactViews.FactListView.as_view(), name='facts_list'),
    re_path(
        '^facts/(?P<asset_id>{regex})/?$'.format(regex=asset_id),
        FactViews.FactDetailView.as_view(), name='facts_detail'),
    # Models
    re_path('^models/?$', ModelViews.ModelListView.as_view(), name='models_list'),
    re_path(
        '^models/(?P<asset_id>{regex})/?$'.format(regex=asset_id),
        ModelViews.ModelDetailView.as_view(), name='models_detail'),    
]