# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
"""
Views for FLINT model-related functionality
"""
from .SLPView import SLPView, SLPListView, SLPDetailView
import api.slp_util as slp_util

from rest_framework.response import Response
from rest_framework import status as http_status

from django.urls import reverse
from rest_framework.test import APIClient

from api.semantics import Semantics

class ModelView(SLPView):
    def asset_name(self):
        return "Model"

class ModelListView(ModelView, SLPListView):
    """
    SLPListView with adapted POST method
    """
    
    def post(self, request):
        """
        Publish a FLINT model.

        This method loops through all the contents of the model,
        posting each data object separately and registering the asset IDs.
        Then, the model itself is serialized and published,
        referring to its frame assets.
        """ 
        # A lookup for the model frames and their relevant endpoints
        # Such frames are called "frames" in FLINT terminology
        model_frames = {
            "acts": reverse('acts_list'),
            "facts": reverse('facts_list'),
            "duties": reverse('duties_list')
        }
        # A collector for the asset IDs
        asset_ids = {key:[] for key in model_frames}
        # TODO probably should not need the client here,
        #   but be able to call a function internally.
        # Initialize API client
        client = APIClient()
        # Authenticate
        client.force_authenticate(user=request.user)
        # Loop over acts/facts/duties
        for frame_type, url in model_frames.items():
            try:
                frames = request.data[frame_type]
            except KeyError:
                # No frames of this type
                continue
            # Go through all the instances of the given type
            # Post and store the asset IDs
            for frame in frames:
                response = client.post(url, frame, format="json")
                if response.status_code == http_status.HTTP_200_OK:
                    asset_id = response.data
                    asset_ids[frame_type].append(asset_id)
        # Pass asset IDs to Semantics for serialization
        model_rdf = Semantics.serialize_model(asset_ids)
        # Publish serialized model
        try:
            model_asset_id = slp_util.publish_as_user(
                user=request.user,
                payload=model_rdf,
                shape_id=self.get_shape_id()
            )
        except Exception as e:
            return Response("Error while posting model: {}".format(str(e)), status=http_status.HTTP_500_INTERNAL_SERVER_ERROR)
        # Publication was successful
        return Response(model_asset_id, status=http_status.HTTP_200_OK)

class ModelDetailView(ModelView, SLPDetailView):
    pass
    # TODO Retrieval should have a toggle for returning full
    #   asset data vs. asset IDs for the frames.
    # Something like the below:
    #
    # def get(self, *args, **kwargs):
    #     as_data = slp_util.parseQueryArgBool("asData")
    #     if as_data:
    #         for asset_id in <model>:
    #             <return object>[asset_id].append(<retrieve asset data>)
    #     else:
    #         # NOTE how to get the correct super class?
    #         return super().get(*args, **kwargs)