# Copyright Nederlandse Organisatie voor Toegepast Natuur-wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for applied scientific research
from .SLPView import SLPView, SLPListView, SLPDetailView

class DutyView(SLPView):
    def asset_name(self):
        return "Duty"

class DutyListView(DutyView, SLPListView):
    pass

class DutyDetailView(DutyView, SLPDetailView):
    pass